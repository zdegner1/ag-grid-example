import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AgGridModule } from "ag-grid-angular";

import { PersonService } from './services/person.service';

import {appRoutes} from './routerConfig';

import { AppComponent } from './components/app.component';
import { PersonGridComponent } from './components/person-grid/person-grid.component';
import { PictureCellRendererComponent } from './components/picture-cell-renderer/picture-cell-renderer.component';

@NgModule({
  declarations: [
    AppComponent,
    PersonGridComponent,
    PictureCellRendererComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AgGridModule.withComponents([
      PictureCellRendererComponent
    ]),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    PersonService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
