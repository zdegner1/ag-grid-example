import { Routes } from '@angular/router';
import { PersonGridComponent } from './components/person-grid/person-grid.component';

export const appRoutes: Routes = [
  {
    path: 'person-grid',
    component: PersonGridComponent
  }
];
