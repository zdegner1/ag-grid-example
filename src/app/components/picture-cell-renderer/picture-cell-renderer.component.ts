import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from "ag-grid-angular";

@Component({
  selector: 'picture-custom-cell-renderer',
  templateUrl: './picture-cell-renderer.component.html',
  styleUrls: ['./picture-cell-renderer.component.css']
})
export class PictureCellRendererComponent implements ICellRendererAngularComp {

  public cellValue: any;

  agInit(cell: any): void {
    this.cellValue = cell.value;
  }

  refresh(cell: any): boolean {
    this.cellValue = cell.value;
    return true;
  }
}
