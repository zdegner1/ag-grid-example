import {Component, ViewEncapsulation} from "@angular/core";
import {ColumnApi, GridApi, GridOptions} from "ag-grid/main";
import {PersonService} from "../../services/person.service"
import {PictureCellRendererComponent} from "../picture-cell-renderer/picture-cell-renderer.component"
import {AgGridColumn} from 'ag-grid-angular';

@Component({
  selector: 'person-grid',
  templateUrl: './person-grid.component.html',
  encapsulation: ViewEncapsulation.None
})
export class PersonGridComponent {

  private gridApi;
  private gridColumnApi;
  public columnDefs: any[];

  public people: any[];


  constructor(private personService: PersonService) {
    this.columnDefs = this.createColumnDefs();

    this.fetchData();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    autoResizeColumnsToFit(this.gridApi);
  }

  private createColumnDefs() : any[] {
    return [
      {
        headerName: "First",
        field: "name.first",
        width: 140,
        editable: true
      },
      {
        headerName: "Last",
        field: "name.last",
        width: 140,
        editable: true
      },
      {
        headerName: "Email",
        field: "email",
        width: 300,
        editable: true
      },
      {
        headerName: "Picture",
        field: "picture",
        cellRendererFramework: PictureCellRendererComponent,
        width: 280,
        autoHeight: true
      },
      {
        headerName: "About",
        field: "about",
        editable: true,
        cellClass: "cell-wrap-text",
        cellEditor: "agLargeTextCellEditor",
        cellEditorParams: {
          maxLength: 10000
        },
        autoHeight: true
      }
    ];
  }

  private fetchData(){
    let people$ = this.personService.getPeople();
    people$.subscribe(res => {
      this.people = res;
    });
  }
}

function autoResizeColumnsToFit(api){
  api.sizeColumnsToFit();
  api.resetRowHeights();

  window.addEventListener("resize", function() {
    setTimeout(function() {
      api.sizeColumnsToFit();
      api.resetRowHeights();
    });
  });
}


